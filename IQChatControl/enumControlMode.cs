﻿/*
    列舉控制項模式
 */

namespace IQChatControl
{
    /// <summary>
    /// 列舉控制項模式
    /// </summary>
    public enum enumControlMode
    {
        /// <summary>
        /// 放大
        /// </summary>
        Full = 0,

        /// <summary>
        /// 精簡
        /// </summary>
        Streamline,

        /// <summary>
        /// 縮小
        /// </summary>
        Mini
    }
}
