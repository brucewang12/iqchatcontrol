﻿using System.Windows.Forms;
using System.Drawing;

namespace IQChatControl
{
    /// <summary>
    /// IQChat WinForm控制項
    /// </summary>
    public partial class IQChatControl : UserControl
    {
        /// <summary>
        /// 控制項當前顯示模式
        /// </summary>
        public enumControlMode ControlMode
        {
            get { return control_mode; }
            set { ChangeMode(value); }
        }
        private enumControlMode control_mode = enumControlMode.Full;

        // 控制項邊框大小
        private static readonly int BORDER_SIZE = 2;

        // 控制項標題最小高度
        private int TITLE_MIN_HEIGHT
        {
            get
            {
                return tableLayoutPanel_title.Height
                    + BORDER_SIZE * 2; // 邊框大小
            }
        }

        // 控制項標題內各控制項間距
        private static readonly int TITLE_PADDING = 5;

        // 控制項標題最小寬度
        private int TITLE_MIN_WIDTH
        {
            get
            {
                // 標題文字內容最小寬度
                const int TITLE_CONTENT_MIN_WIDTH = 100;
                return TITLE_CONTENT_MIN_WIDTH
                    + button_full.Width
                    + button_streamline.Width
                    + button_mini.Width
                    + TITLE_PADDING * 5
                    + BORDER_SIZE * 2; // 邊框大小
            }
        }

        // 精簡模式控制項大小
        private Size STREAMLINED_SIZE
        {
            get
            {
                return new Size(TITLE_MIN_WIDTH, 300);
            }
        }

        #region 控制項模式相關處理

        /// <summary>
        /// 紀錄放大模式時的控制項大小
        /// </summary>
        private Size full_mode_size;

        /// <summary>
        /// 紀錄放大模式時的控制項位置
        /// </summary>
        private Point full_mode_location;

        /// <summary>
        /// 紀錄縮小或精簡模式的X座標位置
        /// </summary>
        private int mini_mode_X = 0;

        /// <summary>
        /// 切換控制項模式
        /// </summary>
        /// <param name="new_mode">新的模式</param>
        private void ChangeMode(enumControlMode new_mode)
        {
            if (control_mode == new_mode) return;

            if (control_mode == enumControlMode.Full)
            {
                full_mode_size = this.Size;
                full_mode_location = this.Location;
            }
            else
            {
                mini_mode_X = this.Location.X;
            }

            control_mode = new_mode;
            switch (new_mode)
            {
                case enumControlMode.Full:
                    {
                        label_title.Text = "IQ-Chat 文字交談系統";
                        this.Size = new Size(full_mode_size.Width, full_mode_size.Height);
                        this.Location = new Point(full_mode_location.X, full_mode_location.Y);
                        break;
                    }
                case enumControlMode.Streamline:
                    {
                        label_title.Text = "IQ-Chat";
                        this.Size = STREAMLINED_SIZE;
                        this.Location = new Point(mini_mode_X, this.Parent.ClientSize.Height - this.Size.Height);
                        break;
                    }
                case enumControlMode.Mini:
                    {
                        label_title.Text = "IQ-Chat";
                        this.Size = new Size(TITLE_MIN_WIDTH, TITLE_MIN_HEIGHT);
                        this.Location = new Point(mini_mode_X, this.Parent.ClientSize.Height - TITLE_MIN_HEIGHT);
                        break;
                    }
            }
        }

        #endregion

        #region 控制項滑鼠移動事件相關處理

        private bool mouse_down = false;
        private Point last_mouse_position;

        private Point GetMouseMoveDisplacement()
        {
            var displacement = new Point(Cursor.Position.X - last_mouse_position.X
                , Cursor.Position.Y - last_mouse_position.Y);
            last_mouse_position = Cursor.Position;
            return displacement;
        }

        #endregion

        #region 控制項改變位置相關處理

        private void ControlMove(enumControlMode mode, Point displacement)
        {
            Point new_location = new Point(this.Location.X, this.Location.Y);

            switch (mode)
            {
                case enumControlMode.Full:
                    {
                        new_location.X += displacement.X;
                        new_location.Y += displacement.Y;
                        break;
                    }
                case enumControlMode.Streamline:
                    {
                        new_location.X += displacement.X;
                        break;
                    }
                case enumControlMode.Mini:
                    {
                        new_location.X += displacement.X;
                        break;
                    }
            }
            this.Location = LimitControlMove(new_location);
        }

        private Point LimitControlMove(Point new_location)
        {
            if (new_location.X < 0)
            {
                new_location.X = 0;
            }
            else if (new_location.X > this.Parent.ClientSize.Width - this.Size.Width)
            {
                new_location.X = this.Parent.ClientSize.Width - this.Size.Width;
            }

            if (new_location.Y < 0)
            {
                new_location.Y = 0;
            }
            else if (new_location.Y > this.Parent.ClientSize.Height - this.Size.Height)
            {
                new_location.Y = this.Parent.ClientSize.Height - this.Size.Height;
            }
            return new_location;
        }

        #endregion

        #region 控制項大小縮放相關處理

        /// <summary>
        /// 列舉控制項縮放方向
        /// </summary>
        private enum ResizeMode
        {
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight,
            Top,
            Bottom,
            Left,
            Right
        }
        private ResizeMode resize_mode;

        private void ChangeCursor(MouseEventArgs e)
        {
            const int detect_range = 10;
            if (e.X < detect_range && e.Y < detect_range)
            {
                resize_mode = ResizeMode.TopLeft;
                Cursor = Cursors.SizeNWSE;
            }
            else if (e.X > this.Size.Width - detect_range && e.Y > this.Size.Height - detect_range)
            {
                resize_mode = ResizeMode.BottomRight;
                Cursor = Cursors.SizeNWSE;
            }
            else if (e.X < detect_range && e.Y > this.Size.Height - detect_range)
            {
                resize_mode = ResizeMode.BottomLeft;
                Cursor = Cursors.SizeNESW;
            }
            else if (e.X > this.Size.Width - detect_range && e.Y < detect_range)
            {
                resize_mode = ResizeMode.TopRight;
                Cursor = Cursors.SizeNESW;
            }
            else if (e.X < detect_range)
            {
                resize_mode = ResizeMode.Left;
                Cursor = Cursors.SizeWE;
            }
            else if (e.X > this.Size.Width - detect_range)
            {
                resize_mode = ResizeMode.Right;
                Cursor = Cursors.SizeWE;
            }
            else if (e.Y < detect_range)
            {
                resize_mode = ResizeMode.Top;
                Cursor = Cursors.SizeNS;
            }
            else if (e.Y > this.Size.Height - detect_range)
            {
                resize_mode = ResizeMode.Bottom;
                Cursor = Cursors.SizeNS;
            }
            else
            {
                Cursor = Cursors.Default;
            }
        }

        private void ControlResize(ResizeMode mode, Point displacement)
        {
            Size new_size = new Size(this.Size.Width, this.Size.Height);
            Point new_location = new Point(this.Location.X, this.Location.Y);
            switch (mode)
            {
                case ResizeMode.TopLeft:
                    {
                        if (this.Location.X != 0 || displacement.X > 0)
                        {
                            new_size.Width -= displacement.X;
                        }
                        if (this.Location.Y != 0 || displacement.Y > 0)
                        {
                            new_size.Height -= displacement.Y;
                        }
                        new_location.X += displacement.X;
                        new_location.Y += displacement.Y;
                        break;
                    }
                case ResizeMode.TopRight:
                    {
                        new_size.Width += displacement.X;
                        if (this.Location.Y != 0 || displacement.Y > 0)
                        {
                            new_size.Height -= displacement.Y;
                        }
                        new_location.Y += displacement.Y;
                        break;
                    }
                case ResizeMode.BottomLeft:
                    {
                        if (this.Location.X != 0 || displacement.X > 0)
                        {
                            new_size.Width -= displacement.X;
                        }
                        new_size.Height += displacement.Y;
                        new_location.X += displacement.X;
                        break;
                    }
                case ResizeMode.BottomRight:
                    {
                        new_size.Width += displacement.X;
                        new_size.Height += displacement.Y;
                        break;
                    }
                case ResizeMode.Top:
                    {
                        if (this.Location.Y != 0 || displacement.Y > 0)
                        {
                            new_size.Height -= displacement.Y;
                        }
                        new_location.Y += displacement.Y;
                        break;
                    }
                case ResizeMode.Bottom:
                    {
                        new_size.Height += displacement.Y;
                        break;
                    }
                case ResizeMode.Left:
                    {
                        if (this.Location.X != 0 || displacement.X > 0)
                        {
                            new_size.Width -= displacement.X;
                        }
                        new_location.X += displacement.X;
                        break;
                    }
                case ResizeMode.Right:
                    {
                        new_size.Width += displacement.X;
                        break;
                    }
            }

            this.Size = LimitControlSize(new_size);
            this.Location = LimitControlLocation(new_location);
        }

        private Point LimitControlLocation(Point new_location)
        {
            if (new_location.X < 0)
            {
                new_location.X = 0;
            }
            else if (new_location.X > this.Location.X + this.Width - TITLE_MIN_WIDTH)
            {
                new_location.X = this.Location.X + this.Width - TITLE_MIN_WIDTH;
            }

            if (new_location.Y < 0)
            {
                new_location.Y = 0;
            }
            else if (new_location.Y > this.Location.Y + this.Height - TITLE_MIN_HEIGHT)
            {
                new_location.Y = this.Location.Y + this.Height - TITLE_MIN_HEIGHT;
            }

            return new_location;
        }

        private Size LimitControlSize(Size new_size)
        {
            if (new_size.Width > this.Parent.ClientSize.Width - this.Location.X)
            {
                new_size.Width = this.Parent.ClientSize.Width - this.Location.X;
            }
            else if (new_size.Width < TITLE_MIN_WIDTH)
            {
                new_size.Width = TITLE_MIN_WIDTH;
            }

            if (new_size.Height > this.Parent.ClientSize.Height - this.Location.Y)
            {
                new_size.Height = this.Parent.ClientSize.Height - this.Location.Y;
            }
            else if (new_size.Height < TITLE_MIN_HEIGHT)
            {
                new_size.Height = TITLE_MIN_HEIGHT;
            }
            return new_size;
        }

        #endregion

        /// <summary>
        /// 控制項建構函式
        /// </summary>
        public IQChatControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 處理畫面更新時控制項閃爍的問題
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void Label_title_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                last_mouse_position = Cursor.Position;
                mouse_down = true;
            }
        }

        private void Label_title_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) mouse_down = false;
        }

        private void Label_title_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouse_down)
            {
                ControlMove(control_mode, GetMouseMoveDisplacement());
            }
        }

        private void TableLayoutPanel_main_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouse_down && control_mode == enumControlMode.Full)
            {
                ChangeCursor(e);
            }
            else if (Cursor != Cursors.Default)
            {
                ControlResize(resize_mode, GetMouseMoveDisplacement());
            }
        }

        private void TableLayoutPanel_main_MouseLeave(object sender, System.EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void TableLayoutPanel_main_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                last_mouse_position = Cursor.Position;
                mouse_down = true;
            }
        }

        private void TableLayoutPanel_main_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) mouse_down = false;
        }

        private void Button_mini_MouseClick(object sender, MouseEventArgs e)
        {
            ChangeMode(enumControlMode.Mini);
        }

        private void Button_full_MouseClick(object sender, MouseEventArgs e)
        {
            ChangeMode(enumControlMode.Full);
        }

        private void Button_streamline_MouseClick(object sender, MouseEventArgs e)
        {
            ChangeMode(enumControlMode.Streamline);
        }
    }
}
