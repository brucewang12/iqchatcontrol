﻿namespace IQChatControl
{
    partial class IQChatControl
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel_main = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel_title = new System.Windows.Forms.TableLayoutPanel();
            this.label_title = new System.Windows.Forms.Label();
            this.button_mini = new System.Windows.Forms.Button();
            this.button_streamline = new System.Windows.Forms.Button();
            this.button_full = new System.Windows.Forms.Button();
            this.tableLayoutPanel_main.SuspendLayout();
            this.tableLayoutPanel_title.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel_main
            // 
            this.tableLayoutPanel_main.AutoSize = true;
            this.tableLayoutPanel_main.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel_main.ColumnCount = 1;
            this.tableLayoutPanel_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel_main.Controls.Add(this.tableLayoutPanel_title, 0, 0);
            this.tableLayoutPanel_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_main.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel_main.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel_main.Name = "tableLayoutPanel_main";
            this.tableLayoutPanel_main.RowCount = 2;
            this.tableLayoutPanel_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.38103F));
            this.tableLayoutPanel_main.Size = new System.Drawing.Size(880, 557);
            this.tableLayoutPanel_main.TabIndex = 0;
            this.tableLayoutPanel_main.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TableLayoutPanel_main_MouseDown);
            this.tableLayoutPanel_main.MouseLeave += new System.EventHandler(this.TableLayoutPanel_main_MouseLeave);
            this.tableLayoutPanel_main.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TableLayoutPanel_main_MouseMove);
            this.tableLayoutPanel_main.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TableLayoutPanel_main_MouseUp);
            // 
            // tableLayoutPanel_title
            // 
            this.tableLayoutPanel_title.AutoSize = true;
            this.tableLayoutPanel_title.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tableLayoutPanel_title.ColumnCount = 9;
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel_title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_title.Controls.Add(this.label_title, 1, 1);
            this.tableLayoutPanel_title.Controls.Add(this.button_mini, 3, 1);
            this.tableLayoutPanel_title.Controls.Add(this.button_streamline, 5, 1);
            this.tableLayoutPanel_title.Controls.Add(this.button_full, 7, 1);
            this.tableLayoutPanel_title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_title.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel_title.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel_title.Name = "tableLayoutPanel_title";
            this.tableLayoutPanel_title.RowCount = 3;
            this.tableLayoutPanel_title.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_title.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel_title.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel_title.Size = new System.Drawing.Size(878, 40);
            this.tableLayoutPanel_title.TabIndex = 0;
            // 
            // label_title
            // 
            this.label_title.AutoSize = true;
            this.label_title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_title.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_title.Location = new System.Drawing.Point(5, 5);
            this.label_title.Margin = new System.Windows.Forms.Padding(0);
            this.label_title.Name = "label_title";
            this.label_title.Size = new System.Drawing.Size(733, 30);
            this.label_title.TabIndex = 0;
            this.label_title.Text = "IQ-Chat 文字交談系統";
            this.label_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label_title.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_title_MouseDown);
            this.label_title.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Label_title_MouseMove);
            this.label_title.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Label_title_MouseUp);
            // 
            // button_mini
            // 
            this.button_mini.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_mini.FlatAppearance.BorderSize = 0;
            this.button_mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_mini.Location = new System.Drawing.Point(743, 5);
            this.button_mini.Margin = new System.Windows.Forms.Padding(0);
            this.button_mini.Name = "button_mini";
            this.button_mini.Size = new System.Drawing.Size(40, 30);
            this.button_mini.TabIndex = 1;
            this.button_mini.Text = "Mini";
            this.button_mini.UseVisualStyleBackColor = true;
            this.button_mini.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_mini_MouseClick);
            // 
            // button_streamline
            // 
            this.button_streamline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_streamline.FlatAppearance.BorderSize = 0;
            this.button_streamline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_streamline.Location = new System.Drawing.Point(788, 5);
            this.button_streamline.Margin = new System.Windows.Forms.Padding(0);
            this.button_streamline.Name = "button_streamline";
            this.button_streamline.Size = new System.Drawing.Size(40, 30);
            this.button_streamline.TabIndex = 2;
            this.button_streamline.Text = "Streamline";
            this.button_streamline.UseVisualStyleBackColor = true;
            this.button_streamline.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_streamline_MouseClick);
            // 
            // button_full
            // 
            this.button_full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_full.FlatAppearance.BorderSize = 0;
            this.button_full.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_full.Location = new System.Drawing.Point(833, 5);
            this.button_full.Margin = new System.Windows.Forms.Padding(0);
            this.button_full.Name = "button_full";
            this.button_full.Size = new System.Drawing.Size(40, 30);
            this.button_full.TabIndex = 3;
            this.button_full.Text = "Full";
            this.button_full.UseVisualStyleBackColor = true;
            this.button_full.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button_full_MouseClick);
            // 
            // IQChatControl
            // 
            this.AccessibleDescription = "IQChatControl";
            this.AccessibleName = "IQChatControl";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel_main);
            this.Name = "IQChatControl";
            this.Size = new System.Drawing.Size(880, 557);
            this.tableLayoutPanel_main.ResumeLayout(false);
            this.tableLayoutPanel_main.PerformLayout();
            this.tableLayoutPanel_title.ResumeLayout(false);
            this.tableLayoutPanel_title.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_main;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_title;
        private System.Windows.Forms.Label label_title;
        private System.Windows.Forms.Button button_mini;
        private System.Windows.Forms.Button button_streamline;
        private System.Windows.Forms.Button button_full;
    }
}
