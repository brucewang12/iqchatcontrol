﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoForm
{
    public partial class DemoForm : Form
    {
        public DemoForm()
        {
            InitializeComponent();

            iqChatControl.ControlMode = IQChatControl.enumControlMode.Mini;
        }
    }
}
