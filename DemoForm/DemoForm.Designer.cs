﻿
namespace DemoForm
{
    partial class DemoForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.iqChatControl = new IQChatControl.IQChatControl();
            this.SuspendLayout();
            // 
            // iqChatControl
            // 
            this.iqChatControl.AccessibleDescription = "IQChatControl";
            this.iqChatControl.AccessibleName = "IQChatControl";
            this.iqChatControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iqChatControl.ControlMode = IQChatControl.enumControlMode.Full;
            this.iqChatControl.Location = new System.Drawing.Point(545, 28);
            this.iqChatControl.Name = "iqChatControl";
            this.iqChatControl.Size = new System.Drawing.Size(684, 410);
            this.iqChatControl.TabIndex = 0;
            // 
            // DemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1286, 706);
            this.Controls.Add(this.iqChatControl);
            this.Name = "DemoForm";
            this.Text = "DemoForm";
            this.ResumeLayout(false);

        }

        #endregion

        private IQChatControl.IQChatControl iqChatControl;
    }
}

